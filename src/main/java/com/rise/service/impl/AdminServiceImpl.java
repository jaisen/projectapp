package com.rise.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rise.doa.AdminDao;
import com.rise.model.Admin;
import com.rise.service.AdminService;

@Service
public class AdminServiceImpl implements AdminService {
	
	@Autowired
	AdminDao adminDaoImpl;

	public Admin validateAdmin(String email,String password){
		Admin admin = adminDaoImpl.adminLogin(email, password);
		return admin;
	}
	
}
