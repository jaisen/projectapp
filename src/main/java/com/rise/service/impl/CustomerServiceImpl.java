package com.rise.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rise.doa.CustomerDao;
import com.rise.model.Customer;
import com.rise.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService{
	
	@Autowired
	CustomerDao customerDaoImpl;
	
	public void saveOrUpdate(Customer customer) {
		customerDaoImpl.saveOrUpdate(customer);
	}

	public List<Customer> list() {
		return customerDaoImpl.list();
	}

	public void delete(int id) {
		Customer customer = get(id);
		customerDaoImpl.delete(customer);
	}

	public Customer get(int id) {
		return customerDaoImpl.get(id);
	}
	
	public Customer validateCustomerCredential(String email, String password) {
		Customer customer =customerDaoImpl.getCustomerDetailsByEmailAndPassword(email, password);
		return customer;
	}	
	/*public Customer validateCustomerCredential(Customer customer1) {
		Customer customer =customerDaoImpl.getCustomerDetailsByEmailAndPassword(customer1);
		return customer;
	}*/
	
	
}
