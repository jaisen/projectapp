package com.rise.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rise.doa.CategoryDao;
import com.rise.model.Category;
import com.rise.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService{
	
	@Autowired
	CategoryDao categoryDaoImpl;
	
	public void saveOrUpdate(Category category) {
		categoryDaoImpl.saveOrUpdate(category);
	}

	public List<Category> list() {
		return categoryDaoImpl.list();
	}

	public void delete(int id) {
		Category category = get(id);
		categoryDaoImpl.delete(category);
	}

	public Category get(int id) {
		return categoryDaoImpl.get(id);
	}

}
