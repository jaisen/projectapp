package com.rise.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rise.doa.ProductDao;
import com.rise.model.Product;
import com.rise.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDao productDaoImpl;

	public void saveOrUpdate(Product product) {
		productDaoImpl.saveOrUpdate(product);
	}

	public List<Product> list() {
		return productDaoImpl.list();
	}

	public void delete(int id) {
		Product product = get(id);
		productDaoImpl.delete(product);
	}

	public Product get(int id) {
		return productDaoImpl.get(id);
	}

}
