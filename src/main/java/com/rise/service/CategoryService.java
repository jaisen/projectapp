package com.rise.service;

import java.util.List;

import com.rise.model.Category;

public interface CategoryService {
	public void saveOrUpdate(Category category);

	public List<Category> list();

	public void delete(int id);

	public Category get(int id);
}
