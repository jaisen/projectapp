package com.rise.service;

import java.util.List;

import com.rise.model.Product;

public interface ProductService {
	public void saveOrUpdate(Product product);
	public List<Product> list();
	public void delete(int id);
	public Product get(int id);
}
