/*package com.rise.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rise.doa.impl.AddressDaoImpl;
import com.rise.model.Address;
import com.rise.model.Product;

@Service
public class AddressService {
	
	@Autowired
	AddressDaoImpl addressDaoImpl;
	
	public void saveOrUpdate(Address address) {
		addressDaoImpl.saveOrUpdate(address);
	}

	public List<Address> list() {
		return addressDaoImpl.list();
	}

	public void delete(int id) {
		Address address = get(id);
		addressDaoImpl.delete(address);
	}

	public Address get(int id) {
		return addressDaoImpl.get(id);
	}
}
*/