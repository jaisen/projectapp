package com.rise.service;

import java.util.List;

import com.rise.model.Customer;

public interface CustomerService {
	public void saveOrUpdate(Customer customer);
	public List<Customer> list();
	public void delete(int id);
	public Customer get(int id);
	public Customer validateCustomerCredential(String email, String password);
}
