package com.rise.service;

import com.rise.model.Admin;

public interface AdminService {
	public Admin validateAdmin(String email,String password);
}
