package com.rise.doa;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


public abstract class BaseDao<T> {

	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public abstract Class<?> getEntityClass();

	protected Session getSession() {

		return sessionFactory.getCurrentSession();
	}

	@Transactional
	public void saveOrUpdate(T entity) {
		getSession().saveOrUpdate(entity);
	}

	@Transactional
	public void delete(T entity) {
		getSession().delete(entity);
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public T get(int id) {
		return (T) getSession().find(getEntityClass(), id);
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Transactional
	public List<T> list() {
		return getSession().createCriteria(getEntityClass()).list();
	}
	
	/*@SuppressWarnings({ "unchecked", "deprecation" })
	@Transactional
	public Customer getCustomerDetailsByEmailAndPassword(Customer obj) {		
		
		CriteriaBuilder builder = getSession().getCriteriaBuilder();
		CriteriaQuery<Customer> query = builder.createQuery(Customer.class);
		Root<Customer> root = query.from(Customer.class);
		
		Predicate a = builder.equal(root.get("email"), obj.getEmail());
		Predicate b = builder.equal(root.get("password"), obj.getPassword());
		Predicate c = builder.or(a,b);
		
		query.where(c);
		
		Query<Customer> q=getSession().createQuery(query);
		Customer customer=q.getSingleResult();
		!customer.getEmail().isEmpty()
		if(a==null) {
			System.out.println("You r valid user");
		}else{
			System.out.println("You r Invalid user");
		}		
		return customer;
	}*/
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Transactional
	public T getCustomerDetailsByEmailAndPassword(String email, String password) {
		
		Query<?> query = getSession().createQuery(" from Customer where email=?0 and password=?1");
		
		query.setString(0, email);
		query.setString(1, password);
		T cObj = null;
		List<?> list = query.list();
		if (list.size() == 1) {
			cObj = (T) list.get(0);
			System.out.println("You r valid user");
		}
		if (cObj == null) {
			System.out.println("You r invalid user");
		}
		return cObj;
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Transactional
	public T adminLogin(String email, String password) {
		
		Query<?> query = getSession().createQuery(" from Admin where email=?0 and password=?1");
		
		query.setString(0, email);
		query.setString(1, password);
		T cObj = null;
		List<?> list = query.list();
		if (list.size() == 1) {
			cObj = (T) list.get(0);
			System.out.println("You r valid user");
		}
		if (cObj == null) {
			System.out.println("You r invalid user");
		}
		return cObj;
	}
}
