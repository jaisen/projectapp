package com.rise.doa.impl;

import org.springframework.stereotype.Repository;

import com.rise.doa.BaseDao;
import com.rise.model.Customer;

@Repository
public class CustomerDaoImpl extends BaseDao<Customer> {

	@Override
	public Class getEntityClass() {
		// TODO Auto-generated method stub
		return Customer.class;
	}

}
