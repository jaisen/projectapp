package com.rise.doa.impl;

import org.springframework.stereotype.Repository;

import com.rise.doa.BaseDao;
import com.rise.model.Category;

@Repository
public class CategoryDaoImpl extends BaseDao<Category> {

	@Override
	public Class getEntityClass() {
		return  Category.class;
	}

}
