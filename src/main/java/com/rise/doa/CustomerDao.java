package com.rise.doa;

import org.springframework.stereotype.Repository;

import com.rise.model.Customer;

@Repository
public class CustomerDao extends BaseDao<Customer> {

	@Override
	public Class<Customer> getEntityClass() {
		return Customer.class;
	}

	
}
