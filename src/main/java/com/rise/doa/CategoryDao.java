package com.rise.doa;

import org.springframework.stereotype.Repository;

import com.rise.model.Category;

@Repository
public class CategoryDao extends BaseDao<Category> {

	@Override
	public Class<Category> getEntityClass() {
		return  Category.class;
	}

}
