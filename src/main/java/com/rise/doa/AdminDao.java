package com.rise.doa;

import org.springframework.stereotype.Repository;

import com.rise.model.Admin;

@Repository
public class AdminDao extends BaseDao<Admin> {

		@Override
		public Class<Admin> getEntityClass() { 
			return Admin.class;
		}		
}
