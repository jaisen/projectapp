package com.rise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.rise.model.Admin;
import com.rise.service.impl.AdminServiceImpl;

@Controller
/*@RequestMapping("/admin")*/
public class AdminController {
	@Autowired
	AdminServiceImpl adminService;
	
	@RequestMapping(value="/admin",method=RequestMethod.GET)
	public String adminLogin(Model model) {
		model.addAttribute("admin",new Admin());
		return "AdminLogin";
	}
	
	@RequestMapping(value="/adminloginSuccess", method=RequestMethod.POST)
	public ModelAndView loginSucess(@ModelAttribute("admin") Admin adm, BindingResult result){
	
		ModelAndView modelAndView = new ModelAndView("AdminSuccess");
		Admin admin = adminService.validateAdmin(adm.getEmail(),adm.getPassword());
		if(admin!= null){
			modelAndView.addObject("admin", admin);
			return modelAndView;
		}else{
			 modelAndView = new ModelAndView("notFound");
		}
		return modelAndView;
	}
	
	
}

