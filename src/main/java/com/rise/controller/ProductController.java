package com.rise.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.rise.model.Category;
import com.rise.model.Product;
import com.rise.service.impl.CategoryServiceImpl;
import com.rise.service.impl.ProductServiceImpl;

@Controller
@RequestMapping("/product")
public class ProductController {

	@Autowired
	ProductServiceImpl productService;
	@Autowired
	CategoryServiceImpl categoryService;
	
	@RequestMapping("/user")
	public ModelAndView view(ModelAndView model) throws IOException {
		List<Product> productList = productService.list();
		model.addObject("productList", productList);
		List<Category> categoryList = categoryService.list();		
		model.addObject("categoryList", categoryList);
		model.setViewName("user");
		return model;
	}

	@RequestMapping("/list")
	public ModelAndView list(ModelAndView model) throws IOException {
		List<Product> productList = productService.list();
		List<Category> categoryList = categoryService.list();		
		model.addObject("categoryList", categoryList);
		model.addObject("productList", productList);
		model.setViewName("home");
		return model;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView saveOrUpdate(@ModelAttribute Product product, BindingResult bindingResult) {
		productService.saveOrUpdate(product);
		return new ModelAndView("redirect:/product/list");
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam(name = "id") Integer id) {
		productService.delete(id);
		return new ModelAndView("redirect:/product/list");

	}

	@RequestMapping(value = "/productForm", method = RequestMethod.GET)
	public ModelAndView newProduct(@RequestParam(name="id", required=false) Integer id, ModelAndView model) {
		Product product = new Product();
		if(id != null) {
			product = productService.get(id);
		}
		List<Category> categoryList = categoryService.list();
		model.addObject("product", product);
		model.addObject("categoryList", categoryList);
		model.setViewName("ProductForm");
		return model;
	}

	
}
