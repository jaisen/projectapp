package com.rise.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.rise.model.Category;
import com.rise.service.impl.CategoryServiceImpl;

@Controller
@RequestMapping("/category")
public class CategoryController {
	
	@Autowired
	CategoryServiceImpl categoryService;
	
	@RequestMapping("/list")
	public ModelAndView list(ModelAndView model) throws IOException {
		List<Category> categoryList = categoryService.list();
		model.addObject("categoryList", categoryList);
		model.setViewName("categoryView");
		return model;
	}

	@RequestMapping(value = "/saveCategory", method = RequestMethod.POST)
	public ModelAndView saveOrUpdate(@ModelAttribute Category category, BindingResult bindingResult) {
		categoryService.saveOrUpdate(category);
		return new ModelAndView("redirect:/category/list");
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam(name = "id") Integer id) {
		categoryService.delete(id);
		return new ModelAndView("redirect:/category/list");

	}

	@RequestMapping(value = "/categoryForm", method = RequestMethod.GET)
	public ModelAndView newProduct(@RequestParam(name="id", required=false) Integer id, ModelAndView model) {
		Category category = new Category();
		if(id != null) {
			category = categoryService.get(id);
		}
		List<Category> categoryList = categoryService.list();
		model.addObject("category", category);
		model.addObject("categoryList", categoryList);
		model.setViewName("CategoryForm");
		return model;
	}

}
