package com.rise.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.rise.model.Address;
import com.rise.model.Customer;
import com.rise.service.impl.CustomerServiceImpl;

@Controller
public class CustomerController {

	@Autowired
	CustomerServiceImpl customerService;

	@RequestMapping(value = "/RegisterForm", method = RequestMethod.GET)
	public ModelAndView newCustomer(@RequestParam(name = "id", required = false) Integer id, ModelAndView model) {
		Customer customer = new Customer();
		if (id != null) {
			customer = customerService.get(id);
		}
		Address address = new Address();
		model.addObject("customer", customer);
		model.addObject("address", address);
		model.setViewName("RegisterForm");
		return model;
	}

	@RequestMapping(value = "/UpdateForm", method = RequestMethod.GET)
	public ModelAndView UpdateCustomer(@RequestParam(name = "id", required = false) Integer id, ModelAndView model) {
		Customer customer = new Customer();
		if (id != null) {
			customer = customerService.get(id);
		}
		Address address = new Address();
		model.addObject("customer", customer);
		model.addObject("address", address);
		model.setViewName("UpdateForm");
		return model;
	}

	@RequestMapping(value = "/registerSave", method = RequestMethod.POST)
	public ModelAndView saveOrUpdate(@ModelAttribute Customer customer, BindingResult bindingResult) {
		customerService.saveOrUpdate(customer);
		return new ModelAndView("redirect:product/user");
	}

	@RequestMapping(value = "/updateCustomer", method = RequestMethod.POST)
	public ModelAndView Update(@ModelAttribute Customer customer, BindingResult bindingResult) {
		customerService.saveOrUpdate(customer);
		return new ModelAndView("redirect:CustomerList");
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		model.addAttribute("cus", new Customer());
		return "login";
	}

	@RequestMapping(value = "/loginSuccess", method = RequestMethod.POST)
	public ModelAndView loginSucess(@ModelAttribute("cus") Customer cusCred, BindingResult result) {

		ModelAndView modelAndView = new ModelAndView("redirect:/product/user");
		Customer cus = customerService.validateCustomerCredential(cusCred.getEmail(),cusCred.getPassword());
		if (cus != null) {
			modelAndView.addObject("cus", cus);
			return modelAndView;
		} else {
			modelAndView = new ModelAndView("notFound");
		}
		return modelAndView;
	}

	@RequestMapping("/CustomerList")
	public ModelAndView list(ModelAndView model) throws IOException {
		List<Customer> customertList = customerService.list();
		Address address = new Address();
		model.addObject("address", address);
		model.addObject("customertList", customertList);
		model.setViewName("CustomerView");
		return model;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam(name = "id") Integer id) {
		customerService.delete(id);
		return new ModelAndView("redirect:/CustomerList");

	}
}
