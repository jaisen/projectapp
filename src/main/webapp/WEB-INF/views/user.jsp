<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<title>All Product</title>

<style type="text/css">
* {
	box-sizing: border-box;
}

.vitrine {
	display: block;
	margin-top: 30px;	
	text-align: center;
	width: 100%;
	font-size: 40px;
}

.user{
	font-size:50px;
	float:right;
	margin-right:90px;
}
.user h6{
	padding:0;
	margin:0;
}
body {
	background: #fff;
	font-family: "Rubik", sans-serif;
	font-size: 13px;
	margin: 0;
	padding: 0;
}

ul {
	padding: 0;
	margin: 0;
}

ul li {
	list-style: none;
}

.product-list {
	align-items: center;
	flex-wrap: wrap;
	display: flex;
	justify-content: center;
	margin: 0 auto;
	max-width: 1300px;
	width: 100%;
}

@media ( max-width : 991px) {
	.product-list {
		flex-wrap: wrap;
	}
}

.product-item {
	border: 1px solid #e1e1e1;
	cursor: pointer;
	margin: 15px 5px;
	max-width: 300px;
	transition: 0.2s;
	width: 100%;
	box-shadow: 0px 0px 25px rgba(0, 0, 0, 0.25);
}

.product-item .product-info {
	margin: 0;
	padding: 0;
	position: relative;
	width: 100%;
}

.product-item .product-info .tag-procent {
	background: #f00;
	color: #fff;
	font-size: 12px;
	font-weight: bold;
	padding: 5px 15px;
	position: absolute;
	right: 10px;
	top: 10px;
}

.product-item .product-info-img {
	border-bottom: 1px solid #e1e1e1;
	height: 240px;
	width: 100%;
	align-items: center;
	display: flex;
	justify-content: center;
}

.product-item .product-info-img img {
	max-width: 200px;
}

.product-item .product-info-description {
	padding: 15px;
}

.product-item .product-info-description .title {
	font-size: 13px;
	margin: 0;
	padding: 0;
}

.product-item .product-info-description .description {
	height: 30px;
	margin: 3px 0;
	-webkit-box-orient: vertical;
	-webkit-line-clamp: 2;
	display: -webkit-box;
	overflow: hidden;
}

.product-item .price-box {
	align-items: center;
	border-top: 1px solid #e1e1e1;
	display: flex;
	justify-content: space-between;
	padding: 15px;
}

.product-item .price ins {
	color: #4d61fc;
	font-size: 18px;
	font-weight: bold;
	text-decoration: none;
}

.product-item:hover {
	transform: translateY(-2px);
	box-shadow: -2px 48px 56px -41px rgba(0, 0, 0, 0.37);
}

.category {
	padding-left: 60px;
}
/*.btn-buy {
  background: #4d61fc;
  border: 0;
  color: #fff;
  cursor: pointer;
  padding: 10px 15px;
  text-decoration: none;
  transition: 0.2s;
}
.btn-buy .fas {
  margin-left: 3px;
}
.btn-buy:hover {
  background: #0c1979;
}
*/
.dropbtn {
	background-color: #4CAF50;
	color: white;
	padding: 16px;
	font-size: 16px;
	border: none;
}

.dropdown {
	position: relative;
	display: inline-block;
}

.dropdown-content {
	display: none;
	position: absolute;
	background-color: #f1f1f1;
	min-width: 160px;
	box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
	z-index: 1;
}

.dropdown-content a {
	color: black;
	padding: 12px 16px;
	text-decoration: none;
	display: block;
}

.dropdown-content a:hover {
	background-color: #ddd;
}

.dropdown:hover .dropdown-content {
	display: block;
}

.dropdown:hover .dropbtn {
	background-color: #3e8e41;
}
.user a{
	font-size:30px;
}
</style>
</head>
<body>

	<script src="https://kit.fontawesome.com/af562a2a63.js"></script>
	<div>
		<h2 class="vitrine">Product List</h2>
		<div class="user">
			<a href="../login">LogOut</a>
			<i class="fas fa-user"></i>			
		</div>
	</div>
	<div class="category">
		<div class="dropdown">
			<h2 class="title">Select Category:</h2>
			<button class="dropbtn">Select</button>
			<div class="dropdown-content">
				<c:forEach items="${categoryList}" var="cat">
					<a href="#">${cat.categoryName}</a>
				</c:forEach>
			</div>
		</div>
	</div>
	<ul class="product-list">
		<c:forEach var="product" items="${productList}">
			<li class="product-item"><figure class="product-info">
					<div class="product-info-img">
						<img src="https://i.ibb.co/2Pvg3yh/images.jpg"
							alt="description image">
					</div>
					<figcaption class="product-info-description">
						<h2 class="title">${product.name}</h2>
						<p class="description">${product.detail}</p>
					</figcaption>
					<div class="price-box">
						<div class="price">
							<div>
								<ins>Rs.${product.price}</ins>
							</div>
						</div>
						<!-- <button href="#" class="btn btn-buy">Buy now <i class="fas fa-arrow-right"></i></button> -->
					</div>
				</figure></li>
		</c:forEach>
	</ul>

	<div
		style="margin: 50px 0; display: flex; align-items: center; justify-content: center; width: 100%">
		<a
			style="display: block; text-align: center; color: #555; text-decoration: none;"
			target="_blank"></a>
	</div>

</body>
</html>