<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<title>Customer List</title>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>
	<div align="center">
		<h1>Customer List</h1>
		
		<table border="5" cellspacing="5" height="200"
			width="1000">
			<tr>
				<th>Customer Id</th>
				<th>Customer FirstName</th>
				<th>Customer LastName</th>
				<th>Email</th>
				<th>Mobile</th>
				<th>Address</th>
				<th>Zipcode</th>
				<th>City</th>
				<th>State</th>				
				<th>Action</th>
			</tr>
			<c:forEach var="customer" items="${customertList}">
				<tr align="center">
					<td>${customer.id}</td>
					<td>${customer.firstName}</td>
					<td>${customer.lastName}</td>
					<td>${customer.email}</td>
					<td>${customer.mobile}</td>
					<td>${customer.address.addressLine}</td>
					<td>${customer.address.zipcode}</td>
					<td>${customer.address.city}</td>
					<td>${customer.address.state}</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp; <a
						href="UpdateForm?id=${customer.id}">Edit</a>
						&nbsp;&nbsp;&nbsp;&nbsp; <a href="delete?id=${customer.id}">Delete</a>
					</td>
				</tr>
			</c:forEach>
		</table>
		
	</div>
</body>
</html>