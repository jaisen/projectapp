<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Shopping Portal</title>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>

<body>
	<div align="center">
		<h1>Category List</h1>
		<h3>
			<a href="categoryForm">Add New Category</a>
		</h3>
		<table border="5" cellspacing="5" height="200"
			width="1000">
			<tr>
				<th>Category Id</th>
				<th>Category Name</th>
				<th>Action</th>
			</tr>
			<c:forEach var="category" items="${categoryList}">
				<tr align="center">
					<td>${category.categoryId}</td>
					<td>${category.categoryName}</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp; <a
						href="categoryForm?id=${category.categoryId}">Edit</a>
						&nbsp;&nbsp;&nbsp;&nbsp; <a href="delete?id=${category.categoryId}">Delete</a>
					</td>
				</tr>
			</c:forEach>
		</table>
		<h3 >
			<a href="<c:url value='/product/list' />" >Product List</a>
		</h3>
	</div>
</body>
</html>