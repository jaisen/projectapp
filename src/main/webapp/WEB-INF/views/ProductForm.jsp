<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 100%;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

div {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}
</style>
</head>
<body>
	<div align="center">
		<h1>Product List</h1>
		<form:form action="save" method="post" modelAttribute="product">

			<table border="5" bgcolor="darkgray" cellspacing="5" height="200"
			width="1000">
				<form:hidden path="id" />
				<tr>
					<td>Product Name:</td>
					<td><form:input path="name" /></td>
				</tr>
				<tr>
					<td>Product Detail:</td>
					<td><form:input path="detail" /></td>
				</tr>
				<tr>
					<td>Product Price:</td>
					<td><form:input path="price" /></td>
				</tr>
				<tr>
					<td>Status:</td>
					<td><form:input path="status" /></td>
				</tr>
				<tr>
					<td>Category Name:</td>
					<td><form:select path="category.categoryId">
							<option selected="selected" disabled="disabled">select</option>
							<c:forEach items="${categoryList}" var="cat">
								<form:option value="${cat.categoryId}">${cat.categoryName}</form:option>
							</c:forEach>
					</form:select></td>
				</tr>
				<tr>
					<td>Add Product Image:</td>
					<td><form:input type="file" path="image" /></td>									
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit"
						value="Save"></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>