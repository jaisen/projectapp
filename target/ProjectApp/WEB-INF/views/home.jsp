<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Shopping Portal</title>

<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>

<body>
	<div align="center">
		<h1>Product List</h1>
		<h3>
			<a href="productForm">New Product</a>
		</h3>
		<table border="5" cellspacing="5" height="200"
			width="1000">
			<tr>
				<th>Product Id</th>
				<th>Product Image</th>
				<th>Product Name</th>
				<th>Product Detail</th>
				<th>Product Price</th>
				<th>Status</th>
				<th>Category</th>
				<th>Action</th>
			</tr>
			<c:forEach var="product" items="${productList}">
				<tr align="center">
					<td>${product.id}</td>
					<td>${product.image}</td>
					<td>${product.name}</td>
					<td>${product.detail}</td>
					<td>${product.price}</td>
					<td>${product.status}</td>
					<td>${product.category.categoryName}</td>
					<td>
					    &nbsp;&nbsp;&nbsp;&nbsp;
					    <a href="productForm?id=${product.id}">Edit</a>
						&nbsp;&nbsp;&nbsp;&nbsp; 
						<a href="delete?id=${product.id}" >Delete</a>
					</td>
				</tr>
			</c:forEach>
		</table>
	
		<h3 >
			<a href="<c:url value='/category/list' />" >View All categories</a>
		</h3>
	</div>
</body>
</html>